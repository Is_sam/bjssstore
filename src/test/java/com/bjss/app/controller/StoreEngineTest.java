package com.bjss.app.controller;

import com.bjss.app.model.Discount;
import com.bjss.app.model.Good;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by issamhammi on 25/02/2018.
 */
public class StoreEngineTest {

    Map<Good, Integer> basket;
    StoreEngine engine;
    Set<Discount> discounts;

    @Before
    public void setup(){
        discounts = new HashSet<>();
        basket = new HashMap<>();
        engine = StoreEngine.getEngine();
    }

    @Test
    public void emptyBasketShouldReturn0() {
        assertEquals(0, engine.subTotal(basket).compareTo(BigDecimal.ZERO));
    }

    @Test
    public void isSubtotalCorrectWithSingleProducts() {
        basket.put(new Good(1L, "Apples", new BigDecimal("1.00")), 1);
        basket.put(new Good(2L, "Bread", new BigDecimal("0.80")), 1);
        basket.put( new Good( 3L, "Milk", new BigDecimal("1.30")), 1);

        BigDecimal expected = new BigDecimal("3.10");
        assertEquals(0, engine.subTotal(basket).compareTo(expected));
    }

    @Test
    public void isSubtotalCorrectWithMultipleProducts() {
        basket.put(new Good(1L, "Apples", new BigDecimal("1.00")), 2);
        basket.put(new Good(2L, "Bread", new BigDecimal("0.80")), 3);
        basket.put( new Good(3L, "Milk", new BigDecimal("1.30")), 1);

        BigDecimal expected = new BigDecimal("5.70");
        assertEquals(0, engine.subTotal(basket).compareTo(expected));
    }

    @Test
    public void isFinalDiscountCorrect() {
        discounts.add(new Discount(1, 1L, new BigDecimal("0.10")));
        basket.put(new Good(1L, "Apples", new BigDecimal("1.00"), discounts), 2);
        basket.put(new Good(2L, "Bread", new BigDecimal("0.80")), 3);
        basket.put( new Good(3L, "Milk", new BigDecimal("1.30")), 1);

        BigDecimal expected = new BigDecimal("0.20");
        assertEquals(0, engine.discount(basket, engine.getDiscounts(basket)).compareTo(expected));
    }

    @Test
    public void isTotalPriceCorrect() {
        discounts.add(new Discount(1, 1L, new BigDecimal("0.10")));
        basket.put(new Good(1L, "Apples", new BigDecimal("1.00"), discounts), 2);
        basket.put(new Good(2L, "Bread", new BigDecimal("0.80")), 3);
        basket.put( new Good(3L, "Milk", new BigDecimal("1.30")), 1);

        BigDecimal expected = new BigDecimal("5.50");
        assertEquals(0,
                engine.subTotal(basket).subtract(
                    engine.discount(basket, engine.getDiscounts(basket))
                ).compareTo(expected));
    }


}