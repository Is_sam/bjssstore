package com.bjss.app.model;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by issamhammi on 25/02/2018.
 */
public class Good {
    //we suppose we are only dealing with prices in GDP
    public static final String CURRENCY = "GDP";

    private final Long id;
    private String name = "default";
    private BigDecimal price = BigDecimal.ZERO;
    private Set<Discount> discounts = new HashSet();

    public Good(Long id) {
        this.id = id;
    }

    public Good(Long id, String name, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public Good(Long id, String name, BigDecimal price, Set<Discount> discounts) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.discounts = discounts;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Set<Discount> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(Set<Discount> discounts) {
        this.discounts = discounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Good good = (Good) o;

        return id.equals(good.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Good{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", discounts=" + discounts +
                '}';
    }
}
