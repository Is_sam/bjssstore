package com.bjss.app.controller;


import com.bjss.app.model.Discount;
import com.bjss.app.model.Good;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by issamhammi on 25/02/2018.
 * Computes prices and discounts
 */
public class StoreEngine {

    private final static Logger logger = Logger.getLogger(StoreEngine.class);


    private StoreEngine() {
    }

    private static final StoreEngine engine = new StoreEngine();

    public static StoreEngine getEngine() {
        return engine;
    }

    /**
     * Calculates the subtotal price of a basket
     * @param basket of goods
     * @return subtotal calculated
     */
    public BigDecimal subTotal(Map<Good, Integer> basket) {
        return basket.entrySet().stream()
                .map(k->k.getKey().getPrice().multiply(new BigDecimal(k.getValue())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * List discounts applicable on the basket with the number of times they happen
     * @param basket
     * @return
     */
    public Map<Discount, Integer> getDiscounts(Map<Good, Integer> basket){
        Map<Discount, Integer> discounts = new HashMap();
        for (Map.Entry<Good, Integer> entry : basket.entrySet()){
            for (Discount discount : entry.getKey().getDiscounts()){
                Integer cummul = entry.getValue()/discount.getQuantity();
                if (cummul.compareTo(0)>0)
                    discounts.put(discount, cummul);
            }
        }
        return discounts;
    }

    /**
     * apply the discounts on the basket
     * @param basket
     * @param discounts
     * @return price to be deduced from subtotal
     */
    public BigDecimal discount(Map<Good, Integer> basket, Map<Discount, Integer> discounts) {
        if (discounts.isEmpty())
            logger.info(String.format("(no offers available)"));
        return discounts.entrySet().stream()
                .map(e -> {
                    BigDecimal subDiscount = BigDecimal.ZERO;
                    Good goodDiscounted = new Good(e.getKey().getGoodIdDiscounted());
                    for(Map.Entry<Good, Integer> entry : basket.entrySet()){
                        if(entry.getKey().equals(goodDiscounted)) {
                            goodDiscounted = entry.getKey();
                            Integer applicableDiscounts = Integer.min(basket.get(goodDiscounted), e.getValue());
                            for(int i = 0; i < applicableDiscounts; i++){
                                subDiscount = subDiscount.add(goodDiscounted.getPrice().multiply(e.getKey().getDiscountPercentage()));
                            }
                            //TODO improve:  what if discount = £10 ?
                            logger.info(String.format("%s %s%% off: -%sp",
                                    goodDiscounted.getName(),
                                    new DecimalFormat("#0").format(e.getKey().getDiscountPercentage().multiply(new BigDecimal("100"))),
                                    new DecimalFormat("#0").format(subDiscount.multiply(new BigDecimal("100")))
                                    ));
                        }
                    }
                    return subDiscount;
                })
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
