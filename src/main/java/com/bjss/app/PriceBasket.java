package com.bjss.app;

import com.bjss.app.controller.StoreEngine;
import com.bjss.app.controller.XMLParser;
import com.bjss.app.model.Discount;
import com.bjss.app.model.Good;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by issamhammi on 25/02/2018.
 */
public class PriceBasket {

    private final static Logger logger = Logger.getLogger(PriceBasket.class);


    public static void main(String[] args) {

        if (args.length == 0) {
            logger.error("No Arguments were found.");
            System.exit(1);
        }

        try {

            //read and parse XML, we store the goods in an HashMap
            XMLParser xmlParser = new XMLParser();
            HashMap<String, Good> goods = xmlParser.parseProducts();

            // read basket from input
            HashMap<Good, Integer> basket = new HashMap();
            for (String arg : args) {
                if (goods.containsKey(arg)) {
                    Good good = goods.get(arg);
                    if (basket.containsKey(good))
                        basket.put(good, basket.get(good) + 1);
                    else
                        basket.put(good, 1);
                } else {
                    logger.error(String.format("Unknown argument: %s", arg));
                    logger.error("argument skipped");
                }

            }

            StoreEngine storeEngine = StoreEngine.getEngine();

            //compute subtotal
            BigDecimal subTotal = storeEngine.subTotal(basket);
            logger.info(String.format("Subtotal: £%s", new DecimalFormat("#0.00").format(subTotal)));

            //compute discounts
            Map<Discount, Integer> discounts = storeEngine.getDiscounts(basket);
            BigDecimal totalDiscount = storeEngine.discount(basket, discounts);

            //compute total price
            BigDecimal totalPrice = subTotal.subtract(totalDiscount);
            logger.info(String.format("Total: £%s", new DecimalFormat("#0.00").format(totalPrice)));

        } catch (ParserConfigurationException | IOException | SAXException e) {
            logger.error(String.format("Error while parsing XML file: %s", e.getMessage()));
            System.exit(1);
        }

        System.exit(0);
    }
}
