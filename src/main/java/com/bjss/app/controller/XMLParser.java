package com.bjss.app.controller;

import com.bjss.app.model.Discount;
import com.bjss.app.model.Good;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by issamhammi on 25/02/2018.
 * Read xml files and return list of goods with possible discounts
 */
public class XMLParser {

    /**
     * Read store.xml file and return a collection of good
     * TODO function way to dense. null management ?
     * @return map of goods
     */
    public HashMap<String, Good> parseProducts() throws ParserConfigurationException, IOException, SAXException {
        final ClassLoader classLoader = getClass().getClassLoader();
        final File file = new File(classLoader.getResource("store.xml").getFile());
        final DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        final Document document = documentBuilder.parse(file);
        HashMap<String, Good> goods = new HashMap();
        document.getDocumentElement().normalize();
        NodeList nodeList = document.getElementsByTagName("good");
        for(int i = 0; i < nodeList.getLength(); i++){
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element element = (Element) node;
                Long id = Long.valueOf(element.getElementsByTagName("id").item(0).getTextContent());
                String name = element.getElementsByTagName("name").item(0).getTextContent();
                String price = element.getElementsByTagName("price").item(0).getTextContent();
                Good good = new Good(id, name, new BigDecimal(price));

                //loop through discounts
                NodeList discounts = element.getElementsByTagName("discount");
                for(int j = 0; j < discounts.getLength(); j++) {
                    Node discount = discounts.item(j);
                    if (discount.getNodeType() == Node.ELEMENT_NODE) {
                        Element discountElement = (Element) discount;
                        Integer quantity = null;
                        Long goodIdDiscounted = null;
                        BigDecimal discountPercentage = null;
                        NodeList conditions = discountElement.getElementsByTagName("condition");
                        for (int k = 0; k < conditions.getLength(); k++) {
                            Node condition = conditions.item(k);
                            if (condition.getNodeType() == Node.ELEMENT_NODE) {
                                Element conditionElement = (Element) condition;
                                NodeList products = conditionElement.getElementsByTagName("product");
                                for (int l = 0; l < products.getLength(); l++) {
                                    Node product = products.item(k);
                                    if (product.getNodeType() == Node.ELEMENT_NODE) {
                                        Element productElement = (Element) product;
                                        quantity = Integer.valueOf(productElement.getElementsByTagName("quantity").item(0).getTextContent());
                                    }
                                }
                            }
                        }
                        NodeList results = discountElement.getElementsByTagName("result");
                        for (int k = 0; k < results.getLength(); k++) {
                            Node result = results.item(k);
                            if (result.getNodeType() == Node.ELEMENT_NODE) {
                                Element resultElement = (Element) result;
                                NodeList products = resultElement.getElementsByTagName("product");
                                for (int l = 0; l < products.getLength(); l++) {
                                    Node product = products.item(k);
                                    if (product.getNodeType() == Node.ELEMENT_NODE) {
                                        Element productElement = (Element) product;
                                        goodIdDiscounted = Long.valueOf(productElement.getElementsByTagName("id").item(0).getTextContent());
                                        discountPercentage = new BigDecimal(productElement.getElementsByTagName("percentage").item(0).getTextContent());
                                    }
                                }
                            }
                        }
                        if (quantity != null && goodIdDiscounted != null && discountPercentage != null)
                            good.getDiscounts().add(new Discount(quantity, goodIdDiscounted, discountPercentage));
                    }
                }
                goods.put(name, good);
            }
        }
        return goods;
    }
}
