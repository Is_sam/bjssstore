package com.bjss.app.model;

import java.math.BigDecimal;

/**
 * Created by issamhammi on 26/02/2018.
 * A discount belongs to a product, it needs the quantity needed for the discount to be applied.
 * Then we need to know for which good the discount is applied to as well as the percentage of the discount
 */
public class Discount {

    private final Integer quantity;
    private final Long goodIdDiscounted;
    private final BigDecimal discountPercentage;

    public Discount(Integer quantity, Long goodIdDiscounted, BigDecimal discountPercentage) {
        this.quantity = quantity;
        this.goodIdDiscounted = goodIdDiscounted;
        this.discountPercentage = discountPercentage;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Long getGoodIdDiscounted() {
        return goodIdDiscounted;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Discount discount1 = (Discount) o;

        if (quantity != null ? !quantity.equals(discount1.quantity) : discount1.quantity != null) return false;
        if (goodIdDiscounted != null ? !goodIdDiscounted.equals(discount1.goodIdDiscounted) : discount1.goodIdDiscounted != null)
            return false;
        return discountPercentage != null ? discountPercentage.equals(discount1.discountPercentage) : discount1.discountPercentage == null;
    }

    @Override
    public int hashCode() {
        int result = quantity != null ? quantity.hashCode() : 0;
        result = 31 * result + (goodIdDiscounted != null ? goodIdDiscounted.hashCode() : 0);
        result = 31 * result + (discountPercentage != null ? discountPercentage.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Discount{" +
                "quantity=" + quantity +
                ", goodIdDiscounted=" + goodIdDiscounted +
                ", discountPercentage=" + discountPercentage +
                '}';
    }
}
